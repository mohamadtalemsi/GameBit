# README - Site Web GameBit

## Description
GameBit, un leader dans la vente de matériel de gaming, de consoles et de jeux vidéo, a sélectionné notre équipe pour concevoir et développer son nouveau site web. Notre mission est de créer une maquette de site web offrant une expérience utilisateur exceptionnelle et de la mettre en œuvre à l'aide des technologies HTML5, CSS3 et JavaScript.

## Table de Contenu
1. [Maquettage](#maquettage)
    - [Prototypage de 6 Pages](#prototypage-de-pages)
    - [Zoning et Wireframes](#zoning-et-wireframes)
    - [Création du Logo](#création-du-logo)
2. [Web Design](#web-design)
    - [Page d'Accueil](#page-daccueil)
    - [Page Catalogue](#page-catalogue)
    - [Page de Personnalisation](#page-de-personnalisation)
    - [Le Panier](#le-panier)
    - [Page Devis](#page-devis)
    - [Page À Propos](#page-à-propos)
    - [Page Contact](#page-contact)
3. [Technologies Utilisées](#technologies-utilisées)
4. [Auteurs](#auteurs)
5. [Liens Figma](#liens-figma)
6. [Planification sur Trello](#planification-sur-trello)


## Maquettage

### Prototypage de 6 Pages
Nous avons créé un prototype comprenant les pages suivantes :
- **Accueil**
- **Catalogue**
- **Page de Personnalisation**
- **Page Devis**
- **À propos**
- **Contact**
Nous avons mis en place un design adaptatif pour garantir une expérience utilisateur optimale sur les appareils de bureau et mobiles.

### Zoning et Wireframes
Nous avons réalisé le zoning et les wireframes du site pour planifier la disposition des éléments et l'agencement des pages.

### Création du Logo
Nous avons proposé un logo qui respecte la charte graphique de GameBit, renforçant ainsi la cohérence visuelle de la marque.

## Web Design

### Page d'Accueil
Nous avons conçu une page d'accueil représentative du site web, mettant en avant l'univers gaming de GameBit.

### Page Catalogue
La page Catalogue regroupe l'ensemble des produits proposés par GameBit, avec une pagination. Les produits sont regroupés par catégories et un système de filtres permet aux utilisateurs de trouver rapidement ce qu'ils cherchent.

### Page de Personnalisation
Sur cette page, les utilisateurs peuvent visualiser en détail un produit sélectionné, ajouter des options supplémentaires, telles que la personnalisation des couleurs, de la mémoire du stockage, du taux de rafraîchissement, etc. Un bouton permet d'ajouter le produit personnalisé au panier, avec un prix ajusté en fonction des fonctionnalités supplémentaires choisies.

### Le Panier
Le panier affiche le total des produits ajoutés par l'utilisateur, avec la possibilité de modifier les quantités. Deux boutons sont présents : un pour valider la commande et un autre pour demander un devis.

### Page Devis
Les utilisateurs peuvent demander un devis avant de finaliser leur commande. Le devis inclut les détails du produit, les prix HT, la TVA, et les montants totaux. La page offre également la possibilité d'imprimer le devis.

### Page À Propos
La page À Propos fournit des informations détaillées sur l'entreprise GameBit, son histoire, des sections de galerie pour les grands événements, et un carrousel mettant en avant les partenaires de l'entreprise.

### Page Contact
La page Contact comprend un formulaire où les utilisateurs peuvent saisir leur nom, prénom, numéro de téléphone, e-mail et message. Après validation, un message de confirmation s'affiche.

## Technologies Utilisées
Ce projet a été développé en utilisant les technologies suivantes :

- **HTML5**: Nous avons utilisé HTML5 pour structurer les pages web et définir leur contenu.

- **CSS3**: Les styles de ce site web ont été créés en utilisant CSS3 pour rendre les pages attrayantes et réactives.

- **JavaScript**: Nous avons implémenté la logique interactivité du site en utilisant JavaScript pour rendre les pages dynamiques.

- **Framework Tailwind CSS**: Le design du site web a été facilité grâce à l'utilisation du framework CSS Tailwind, qui nous a permis de créer rapidement des composants réutilisables avec une approche utility-first.

Nous avons veillé à maintenir des bonnes pratiques de développement et à garantir la compatibilité avec une variété de navigateurs web.


## Auteurs
- [Douaesb](https://github.com/Douaesb)
- [Abdellah Talemsi](https://github.com/ATalemsi)
- [Hiba Beghdi](https://github.com/HIBA-BEG)
- [Imadeddine Saghir](https://github.com/SAGHIR-Imadeddine)
- [Yassine Benbrika](https://github.com/yassinebenbrika)

## Liens Figma
- [Lien vers la maquette Figma](https://www.figma.com/file/ssJOFKwrRsPqNzc6meegSC/Untitled?type=design&node-id=0%3A1&mode=design&t=AQmnaq7YC77NG5fn-1)

## Planification sur Trello
- [Lien vers le tableau de planification sur Trello](https://trello.com/b/7anXkt7F/gamebit

N'hésitez pas à personnaliser ce README en ajoutant plus de détails, des captures d'écran ou d'autres informations pertinentes pour votre projet.
